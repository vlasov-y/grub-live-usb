#!/usr/bin/env sh
set -e

log_error() { printf "\e[5;49;91merror\e[0m: %s\n" "$*"; }
log_info() { printf "\e[1;32minfo\e[0m: %s${INFO_LINE_END:-\n}" "$*"; }

CRYPT="./boot/crypt"
NAME="crypt_file"
MOUNTPOINT="./mount"
ACTION='mount'

while getopts "fnpuh" OPTION; do
    case $OPTION in
    f)	# file
        CRYPT="$OPTARG"
        ;;
    n)	# name of crypt mount
        NAME="$OPTARG"
        ;;
    p)	# path to mount crypt in
        MOUNTPOINT="$OPTARG"
        ;;
    u)	# umount asked path
        ACTION='umount'
        ;;
    h)	# help
        cat <<EOF
crypt.sh [-f <file>] [-n <name>] [-p <mountpoint>] [-u]
-f <file> - path to encrypted file / block device
-n <name> - how will it be named after decription
-p <mountpoint> - where should it be mounted
-u (optional) - umount asked path and close crypt
EOF
        exit 0
        ;;
    *)
        log_error "Incorrect options provided"
        exit 1
        ;;
    esac
done

if [ ! -f "$CRYPT" ]; then
    log_error "File ${CRYPT} does not exist."
    exit 2
fi

if ! (touch /root.test && rm -f /root.test); then
    log_error "Need root permissions."
    exit 3
fi

if [ "$ACTION" = 'mount' ]; then
    log_info "Creating ${MOUNTPOINT} ..."
    mkdir -p "$MOUNTPOINT"
    log_info "Decrypting ${CRYPT} ..."
    cryptsetup luksOpen "$CRYPT" "$NAME"
    log_info "Mounting to ${MOUNTPOINT} ..."
    mount "/dev/mapper/$NAME" "$MOUNTPOINT"
    log_info 'Done.'
elif [ "$ACTION" = 'umount' ]; then
    log_info "Unmounting ${MOUNTPOINT} ..."
    umount "$MOUNTPOINT"
    log_info "Closing ${CRYPT} ..."
    cryptsetup luksClose "$NAME"
    log_info "Removing ${MOUNTPOINT} ..."
    rm -rv "$MOUNTPOINT"
    log_info 'Done.'
fi
