## Simple GRUB2 Live USB  
  
It is handy to have USB which can be used on Windows as simple storage and be the boot USB at the same time without a need in reformatting it. Also, it is good idea to store sensitive data encrypted. Project contains simple GRUB2 configs and crypt file. Default password is ' ' (space).  
  
### How create crypt volume?  
```bash  
F='./crypt'  
S='1'  
dd if=/dev/zero of="$F" bs=1M count="$S"  
cryptsetup luksFormat "$F"  
```  
You can easily mount that file with script _crypt.sh_.  
```bash  
sh ./scripts/crypt.sh -f "$F" -n 'my_crypt' -p '/somewhere/here'  
```  
And umount adding _-u_.  
```bash  
sh ./scripts/crypt.sh -f "$F" -n 'my_crypt' -p '/somewhere/here' -u  
```  
  
### How make bootable USB  
You can see list of block devices with executing _lsblk_. Lets assume that needed block device is /dev/sdb.  
First of all, create NTFS partition in MSDOS table on that drive with GParted or any tool you wish.  
```bash  
sudo gparted /dev/sdb  
```  
Now we have one partition /dev/sdb1. Need to mount it and install GRUB2 (can take a long time).  
```bash  
sudo mount /dev/sdb1 /mnt  
sudo grub-install --boot-directory /mnt/boot /dev/sdb  
```  
Copying configs, images and scripts and umounting.  
```bash  
cp -rvf * /mnt/boot  
mv /mnt/boot/grub.cfg /mnt/boot/grub  
sudo umount /mnt
```  
Now you should be able to boot from that USB stick.  
  